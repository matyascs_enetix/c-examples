﻿using ClassLibrary;
using System;
using System.IO;
using System.Xml.Serialization;

namespace App
{
    class Program
    {
        static void Main(string[] args)
        {
            var order = new PurchaseOrderType
            {
                BillTo = new USAddress
                {
                    city = "California"
                },
                OrderDate = DateTime.Now,
                OrderDateSpecified = true//if false, orderDate won't be serialized
            };

            var serializer = new XmlSerializer(typeof(PurchaseOrderType));
            using (TextWriter writer = new StreamWriter("output.xml"))
            {
                serializer.Serialize(writer, order);
            }


            PurchaseOrderType savedOrder = null;
            using (TextReader reader = new StreamReader("output.xml"))
            {
                savedOrder = (PurchaseOrderType)serializer.Deserialize(reader);
            }

            Console.WriteLine("Order: "+order.OrderDate.ToString());
            Console.WriteLine("Saved order: "+savedOrder.OrderDate.ToString());

            Console.ReadKey();




        }
    }
}
