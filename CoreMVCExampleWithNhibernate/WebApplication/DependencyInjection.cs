﻿using DataLayer.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication
{
    public class DependencyInjection
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddSingleton<DbSessionFactory>();
            services.AddScoped<IContext, DbContext>();
           
        }
    }
}
