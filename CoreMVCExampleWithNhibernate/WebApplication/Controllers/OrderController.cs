﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLayer.Configuration;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NHibernate.Linq;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class OrderController : Controller
    {
        private readonly IContext _context;

        public OrderController(IContext context)
        {
            _context = context;
        }

        // GET: Order
        public ActionResult Index()
        {
            return View(_context.Session.Query<Order>().Select(x => new OrderVM {
                                                                    Id = x.Id,
                                                                    OrderedProduct = x.OrderedProduct,
                                                                    OrderedQuantity = x.OrderedQuantity
                                                                })
                                                       .ToList()
            );
        }

        // GET: Order/Details/(guid)
        public ActionResult Details(Guid id)
        {
            return View();
        }

        // GET: Order/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Order/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OrderVM model)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    var order = new Order
                    {
                        Id = Guid.NewGuid(),
                        OrderDate = DateTime.Now,
                        OrderedProduct = model.OrderedProduct,
                        OrderedQuantity = model.OrderedQuantity
                    };

                    _context.BeginTransaction();
                    _context.Session.Save(order);
                    var success = _context.Commit();
                }

               

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Order/Edit/5
        public ActionResult Edit(Guid id)
        {
            return View(_context.Session.Query<Order>().Where(x => x.Id == id).Select(x => new OrderVM
            {
                Id = x.Id,
                OrderedProduct = x.OrderedProduct,
                OrderedQuantity = x.OrderedQuantity
            })
            .SingleOrDefault());
        }

        // POST: Order/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Guid id, OrderVM model)
        {
            try
            {
                _context.BeginTransaction();
                var order = _context.Session.Get<Order>(id);

                order.OrderedProduct = model.OrderedProduct;
                order.OrderedQuantity = model.OrderedQuantity;

                var success = _context.Commit();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Order/Delete/5
        public ActionResult Delete(Guid id)
        {
            return View();
        }

        // POST: Order/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteOrder(Guid id)
        {
            try
            {
                //_context.Session.Query<Order>().Where(x => x.Id == id).Delete();
                _context.Session.Delete(_context.Session.Get<Order>(id));

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}