﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class OrderVM
    {

        public Guid Id { get; set; }

        [Required]
        [Range(1, Double.MaxValue)]
        [DisplayName("foo")]
        public virtual int OrderedQuantity { get; set; }

        [Required]
        [MaxLength(100)]
        public virtual string OrderedProduct { get; set; }
    }
}
