﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace MigrationRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            string settingsDirectory = Directory.GetCurrentDirectory();
            while (!settingsDirectory.EndsWith("MigrationRunner"))//hacky way to get to the appsettings file
            {
                settingsDirectory = Directory.GetParent(settingsDirectory).FullName;
            }
            var settingsFile = Path.GetFullPath(settingsDirectory + "/../WebApplication/appsettings.json");

            var config = new ConfigurationBuilder()
                  .AddJsonFile(settingsFile, false)
                  .Build();

            var connectionString = config.GetConnectionString("DefaultConnection");

            Migrator.MigrateDown(connectionString);
            Migrator.MigrateUp(connectionString);
            //Migrator.Seed(config);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
