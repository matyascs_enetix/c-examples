﻿using DataLayer.Migrations;
using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigrationRunner
{
    class Migrator
    {     

        public static void MigrateDown(string connectionString)
        {
            var serviceProvider = CreateProvider(connectionString);

            // Put the database update into a scope to ensure
            // that all resources will be disposed.
            using (var scope = serviceProvider.CreateScope())
            {
                // Instantiate the runner
                var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();

                var migrations = runner.MigrationLoader.LoadMigrations();

                foreach (var migration in migrations.OrderByDescending(x => x.Key))
                {
                    // Execute the migration
                    try
                    {
                        runner.MigrateDown(migration.Key);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error downing migration {0}: {1}", migration.Key, e.Message);
                        break;
                    }
                }
            }
        }

        public static void MigrateUp(string connectionString)
        {

            var serviceProvider = CreateProvider(connectionString);

            // Put the database update into a scope to ensure
            // that all resources will be disposed.
            using (var scope = serviceProvider.CreateScope())
            {
                // Instantiate the runner
                var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();

                

                var migrations = runner.MigrationLoader.LoadMigrations();

             
                foreach (var migration in migrations.OrderBy(x => x.Key))
                {
                    // Execute the migration
                    try
                    {
                        runner.MigrateUp(migration.Key);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error upping migration {0}: {1}", migration.Key, e.Message);
                        break;
                    }
                }

            }
        }

        private static ServiceProvider CreateProvider(string connectionString)
        {
            return new ServiceCollection()
                    // Registration of all FluentMigrator-specific services
                    .AddFluentMigratorCore()
                    // Configure the runner
                    .ConfigureRunner(
                        builder => builder
                            .AddPostgres()
                            .WithGlobalConnectionString(connectionString)
                            // Specify the assembly with the migrations
                            .WithMigrationsIn(typeof(_1_InitialOrderMigration).Assembly))
                    .BuildServiceProvider();
        }
    }
}
