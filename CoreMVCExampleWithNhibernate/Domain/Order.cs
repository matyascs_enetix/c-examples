﻿using System;

namespace Domain
{
    public class Order
    {

        public virtual Guid Id { get; set; }

        public virtual int OrderedQuantity { get; set; }
   
        public virtual string OrderedProduct { get; set; }

        public virtual DateTime OrderDate { get; set; }


    }
}
