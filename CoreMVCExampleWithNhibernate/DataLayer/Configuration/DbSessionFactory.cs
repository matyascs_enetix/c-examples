﻿using Microsoft.Extensions.Configuration;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Configuration
{
    public class DbSessionFactory
    {
        private readonly ISessionFactory _sessionFactory;
       
        public DbSessionFactory(IConfiguration config)
        {
            var appConnection = config.GetConnectionString("DefaultConnection");
          
            _sessionFactory = NHibernateConfiguration.CreateConfiguration(appConnection, true)
                .BuildSessionFactory();
        }

        internal ISession OpenSession()
        {
            return _sessionFactory.OpenSession();
        }
    }
}
