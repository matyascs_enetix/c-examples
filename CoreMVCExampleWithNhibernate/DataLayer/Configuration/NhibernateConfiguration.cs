﻿using DataLayer.Mappings;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace DataLayer.Configuration
{
    public class NHibernateConfiguration
    {
        public static FluentConfiguration CreateConfiguration(string connectionString, bool showSql)
        {
            var postgreConf = PostgreSQLConfiguration.PostgreSQL82
                         .Raw("hbm2ddl.keywords", "none")
                         .ConnectionString(c => c.Is(connectionString));
            if (showSql)
            {
                postgreConf.ShowSql();
            }

            var cfg = Fluently.Configure()
                 .Database(postgreConf)
                 .Mappings(m => m.FluentMappings.AddFromAssemblyOf<OrderMap>());

            return cfg;
        }
    }
}
