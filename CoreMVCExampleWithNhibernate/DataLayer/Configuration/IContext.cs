﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Configuration
{
    public interface IContext
    {
        ISession Session { get; }


        void BeginTransaction();


        bool Commit();
    }
}
