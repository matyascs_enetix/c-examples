﻿using Domain;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataLayer.Configuration
{

    public class DbContext : IDisposable, IContext
    {
        private ITransaction _transaction;

        public ISession Session { get; private set; }

        public DbContext(DbSessionFactory sessionFactory)
        {
            Session = sessionFactory.OpenSession();
            Session.FlushMode = FlushMode.Commit;
        }

        public void BeginTransaction()
        {
            _transaction = Session.BeginTransaction(System.Data.IsolationLevel.Serializable);             
        }


        public bool Commit()
        {
            bool success = false;
            try
            {
                // commit transaction if there is one active
                if (_transaction != null && _transaction.IsActive)
                {
                    _transaction.Commit();
                    success = true;
                }
                else
                {
                    success = false;
                }

            }
            catch (Exception e)
            {
                // rollback if there was an exception
                if (_transaction != null && _transaction.IsActive)
                    _transaction.Rollback();
                success = false;
                throw;
            }
            finally
            {
                _transaction.Dispose();
            }
            return success;
        }

        public void Dispose()
        {

            if (_transaction != null)
            {
                if (_transaction.IsActive)
                {
                    _transaction.Rollback();
                }
                _transaction.Dispose();
            }

            Session.Close();
            Session.Dispose();
        }       
    }
}
