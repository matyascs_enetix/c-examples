﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Migrations
{
    [Migration(1)]
    public class _1_InitialOrderMigration : Migration
    {
        public override void Down()
        {
            Delete.Table("orders");
        }

        public override void Up()
        {
            Create.Table("orders")
                .WithColumn("order_id").AsGuid().PrimaryKey()
                .WithColumn("quantity").AsInt32().NotNullable()
                .WithColumn("product").AsString(100).NotNullable()
                .WithColumn("order_date").AsDate().NotNullable()
            ;
        }
    }    
}
