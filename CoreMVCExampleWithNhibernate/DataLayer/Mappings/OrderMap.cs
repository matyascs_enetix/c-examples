﻿using Domain;
using FluentNHibernate.Mapping;

namespace DataLayer.Mappings
{
    public class OrderMap : ClassMap<Order>
    {
        public OrderMap()
        {
            Table("orders");

            Id(x => x.Id, "order_id").GeneratedBy.Assigned();
            Map(x => x.OrderedQuantity, "quantity");
            Map(x => x.OrderedProduct, "product");
            Map(x => x.OrderDate, "order_date");
        }
    }
}
