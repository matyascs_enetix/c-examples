﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PictureViewer
{
    public partial class CustomControl : UserControl
    {

        public string InitialValue { get; set; }

        private List<Entity> entities;

        private int counter = 0;
        private Task backgroundTask;
        private CancellationTokenSource tokenSource;

        public CustomControl()
        {
            InitializeComponent();
            InitialValue = "";
            entities = new List<Entity>();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            var newEntity = new Entity(InitialValue);
            bumpRanks();
            entities.Add(newEntity);

            var input = new TextBox
            {
                Width = 200,
                Height = 40,
            };
            input.DataBindings.Add("Text", newEntity, "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            flowLayoutPanel1.Controls.Add(input);           
        }

        private void bumpRanks()
        {
            foreach (var item in entities)
            {
                item.Rank++;
            }
        }

        private void BackgroundCounter(object token)
        {
            CancellationToken cancelToken = (CancellationToken)token;
            while (!cancelToken.IsCancellationRequested)
            {
                counter++;
                //lblTime.Text = counter.ToString();//not safe

                Invoke(new Action(() =>
                {
                    lblTime.Text = counter.ToString();
                }));
               
                Thread.Sleep(1000);
            }

        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void CustomControl_Load(object sender, EventArgs e)
        {
            tokenSource = new CancellationTokenSource();
            backgroundTask = new Task(BackgroundCounter, tokenSource.Token);
            backgroundTask.Start();
        }
       
    }


    public class Entity : INotifyPropertyChanged
    {
        private string _name;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Name
        {
            get
            {
                return _name + " " + Rank;
            }

            set
            {
                _name = value;
            }
        }

        private int _rank = 0;
        public int Rank
        {
            get
            {
                return _rank;
            }

            set
            {
                _rank = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Name"));
            }
        }

        public Entity(string initialValue)
        {
            Name = initialValue;
        }
    }
}
