## C# Desktop Winforms application(only on windows)
* Form designer
* Adding controls
* Adding event handlers
* Creating custom controls
* Databinding: INotifyPropertyChanged
* Cross thread component access

### Links
* Modified from [Simple winforms app tutorial](https://docs.microsoft.com/en-us/visualstudio/ide/tutorial-1-create-a-picture-viewer?view=vs-2017)