## C# ASP<span></span>.NET (Core) Web application(cross platform-ish)
### Quickstart steps:
* create mvc .net core web app with authentication
* create domain model class
* add controller with views using entity framework
* in the package manager console run Add-Migration "migration name"
* in the package manager console run Update-Database
* add data annotation for validation

### Links
* [ASP.NET Core tutorial](https://www.tutorialspoint.com/asp.net_core/)
* [ASP.NET (Core) sample applications](https://github.com/aspnet/samples)
* [Entity Framework Core tutorial - Code first](https://docs.microsoft.com/en-us/ef/core/get-started/aspnetcore/new-db?tabs=visual-studio)
* [Entity Framework - Database first](https://docs.microsoft.com/en-us/ef/ef6/modeling/designer/workflows/database-first)