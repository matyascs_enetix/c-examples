﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class Document
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Author { get; set; }

        [Required]        
        public Catalog Catalog { get; set; }
    }
}
